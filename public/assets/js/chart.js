
var handleAreaChart = function() {
	"use strict";
	
	var options = {
		chart: {
			height: 350,
			type: 'area',
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth',
			width: 3
		},
		colors: [COLOR_PINK, COLOR_DARK],
		series: [{
			name: 'series1',
			data: [31, 40, 28, 51, 42, 109, 100]
		}],

		xaxis: {
			type: 'datetime',
			categories: ['2019-09-19T18:00:00', '2019-09-19T19:00:00', '2019-09-19T20:00:00', '2019-09-19T03:00:00', '2019-09-19T04:00:00', '2019-09-19T05:00:00', '2019-09-19T06:00:00'],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}             
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm'
			},
		}
	};
	
	var chart = new ApexCharts(
		document.querySelector('#apex-area-chart'),
		options
	);

	chart.render();
};
var handleAreaChart2 = function() {
	"use strict";
	
	var options = {
		chart: {
			height: 350,
			type: 'area',
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth',
			width: 3
		},
		colors: [COLOR_PINK, COLOR_DARK],
		series: [{
			name: 'series1',
			data: [7.58, 6.21, 6.29, 6.11, 7.12, 7.11, 6.50]
		}],

		xaxis: {
			type: 'datetime',
			categories: ['2019-09-19T00:00:00', '2019-09-19T01:00:00', '2019-09-19T02:00:00', '2019-09-19T03:00:00', '2019-09-19T04:00:00', '2019-09-19T05:00:00', '2019-09-19T06:00:00'],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}             
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm'
			},
		}
	};
	
	var chart = new ApexCharts(
		document.querySelector('#apex-area-chart2'),
		options
	);

	chart.render();
};
var handleAreaChart3 = function() {
	"use strict";
	
	var options = {
		chart: {
			height: 350,
			type: 'area',
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth',
			width: 3
		},
		colors: [COLOR_PINK, COLOR_DARK],
		series: [{
			name: 'series1',
			data: [233, 140, 328, 251, 142, 109, 100]
		}],

		xaxis: {
			type: 'datetime',
			categories: ['2019-09-19T00:00:00', '2019-09-19T01:00:00', '2019-09-19T02:00:00', '2019-09-19T03:00:00', '2019-09-19T04:00:00', '2019-09-19T05:00:00', '2019-09-19T06:00:00'],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}             
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm'
			},
		}
	};
	
	var chart = new ApexCharts(
		document.querySelector('#apex-area-chart3'),
		options
	);

	chart.render();
};

var handleAreaChart4 = function() {
	"use strict";
	
	var options = {
		chart: {
			height: 350,
			type: 'area',
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth',
			width: 3
		},
		colors: [COLOR_PINK, COLOR_DARK],
		series: [{
			name: 'series1',
			data: [221, 130, 344, 241, 190, 139, 144]
		}],

		xaxis: {
			type: 'datetime',
			categories: ['2019-09-19T00:00:00', '2019-09-19T01:00:00', '2019-09-19T02:00:00', '2019-09-19T03:00:00', '2019-09-19T04:00:00', '2019-09-19T05:00:00', '2019-09-19T06:00:00'],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}             
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm'
			},
		}
	};
	
	var chart = new ApexCharts(
		document.querySelector('#apex-area-chart4'),
		options
	);

	chart.render();
};

var handleAreaChart5 = function() {
	"use strict";
	
	var options = {
		chart: {
			height: 350,
			type: 'area',
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth',
			width: 3
		},
		colors: [COLOR_PINK, COLOR_DARK],
		series: [{
			name: 'series1',
			data: [31, 40, 28, 51, 42, 109, 100]
		}],

		xaxis: {
			type: 'datetime',
			categories: ['2019-09-19T00:00:00', '2019-09-19T01:00:00', '2019-09-19T02:00:00', '2019-09-19T03:00:00', '2019-09-19T04:00:00', '2019-09-19T05:00:00', '2019-09-19T06:00:00'],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}             
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm'
			},
		}
	};
	
	var chart = new ApexCharts(
		document.querySelector('#apex-area-chart5'),
		options
	);

	chart.render();
};

var handleAreaChart6 = function() {
	"use strict";
	
	var options = {
		chart: {
			height: 350,
			type: 'area',
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth',
			width: 3
		},
		colors: [COLOR_PINK, COLOR_DARK],
		series: [{
			name: 'series1',
			data: [31, 40, 28, 51, 42, 109, 100]
		}],

		xaxis: {
			type: 'datetime',
			categories: ['2019-09-19 00:00:00', '2019-09-19 01:00:00', '2019-09-19T02:00:00', '2019-09-19T03:00:00', '2019-09-19T04:00:00', '2019-09-19T05:00:00', '2019-09-19T06:00:00'],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}             
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm'
			},
		}
	};
	
	var chart = new ApexCharts(
		document.querySelector('#apex-area-chart6'),
		options
	);

	chart.render();
};

var ChartApex = function () {
	"use strict";
	return {
		//main function
		init: function () {
			handleAreaChart();
            handleAreaChart2();
            handleAreaChart3();
            handleAreaChart4();
            handleAreaChart5();
            handleAreaChart6();
		}
	};
}();

$(document).ready(function() {
	ChartApex.init();
});