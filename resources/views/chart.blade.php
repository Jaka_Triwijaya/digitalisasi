
<script>		
	var handleAreaChart = function() {
		"use strict";
		
		var options = {
			chart: {
				height: 350,
				type: 'line',
			},
			dataLabels: {
				enabled: true
			},
			stroke: {
				curve: 'smooth',
				width: 3
			},
			colors: [COLOR_PINK, COLOR_DARK],
			series: [{
				name: 'Flow',
				data: [
					<?php
						foreach ($item as $sensor) {
							$debit = round($sensor->debit,2);
							echo "$debit,";
						}
					?>
				]
			}],

			xaxis: {
				type: 'time',
				categories: [
					// '19:00:00', '19:00:00', '02:00', '03:00', '04:00', '05:00', '06:00',
					<?php
						foreach ($item as $sensor) {
							echo "'$sensor->jam',";
						}
					?>
					
				],
				axisBorder: {
					show: true,
					color: COLOR_SILVER_TRANSPARENT_5,
					height: 1,
					width: '100%',
					offsetX: 0,
					offsetY: -1
				},
				axisTicks: {
					show: true,
					borderType: 'solid',
					color: COLOR_SILVER,
					height: 6,
					offsetX: 0,
					offsetY: 0
				}             
			},
			tooltip: {
				x: {
					format: 'HH:mm:ss'
				},
			}
		};
		
		var chart = new ApexCharts(
			document.querySelector('#apex-area-chart'),
			options
		);

		chart.render();
	};
	var handleAreaChart2 = function() {
		"use strict";
		
		var options = {
			chart: {
				height: 350,
				type: 'line',
			},
			dataLabels: {
				enabled: true
			},
			stroke: {
				curve: 'smooth',
				width: 3
			},
			colors: [COLOR_PINK, COLOR_DARK],
			series: [{
				name: 'PH',
				data: [
					<?php
						foreach ($item as $sensor) {
							$ph = round($sensor->ph,2);
							echo "$ph,";
						}
					?>
				]
			}],

			xaxis: {
				type: 'time',
				categories: [
					<?php
						foreach ($item as $sensor) {
							echo "'$sensor->jam',";
						}
					?>
				],
				axisBorder: {
					show: true,
					color: COLOR_SILVER_TRANSPARENT_5,
					height: 1,
					width: '100%',
					offsetX: 0,
					offsetY: -1
				},
				axisTicks: {
					show: true,
					borderType: 'solid',
					color: COLOR_SILVER,
					height: 6,
					offsetX: 0,
					offsetY: 0
				}             
			},
			tooltip: {
				x: {
					format: 'HH:mm'
				},
			}
		};
		
		var chart = new ApexCharts(
			document.querySelector('#apex-area-chart2'),
			options
		);

		chart.render();
	};
	var handleAreaChart3 = function() {
		"use strict";
		
		var options = {
			chart: {
				height: 350,
				type: 'line',
			},
			dataLabels: {
				enabled: true
			},
			stroke: {
				curve: 'smooth',
				width: 3
			},
			colors: [COLOR_PINK, COLOR_DARK],
			series: [{
				name: 'COD',
				data: [
					<?php
						foreach ($item as $sensor) {
							$cod = round($sensor->cod,2);
							echo "$cod,";
						}
					?>
				]
			}],

			xaxis: {
				type: 'time',
				categories: [
					<?php
						foreach ($item as $sensor) {
							echo "'$sensor->jam',";
						}
					?>
				],
				axisBorder: {
					show: true,
					color: COLOR_SILVER_TRANSPARENT_5,
					height: 1,
					width: '100%',
					offsetX: 0,
					offsetY: -1
				},
				axisTicks: {
					show: true,
					borderType: 'solid',
					color: COLOR_SILVER,
					height: 6,
					offsetX: 0,
					offsetY: 0
				}             
			},
			tooltip: {
				x: {
					format: 'HH:mm'
				},
			}
		};
		
		var chart = new ApexCharts(
			document.querySelector('#apex-area-chart3'),
			options
		);

		chart.render();
	};

	var handleAreaChart4 = function() {
		"use strict";
		
		var options = {
			chart: {
				height: 350,
				type: 'line',
			},
			dataLabels: {
				enabled: true
			},
			stroke: {
				curve: 'smooth',
				width: 3
			},
			colors: [COLOR_PINK, COLOR_DARK],
			series: [{
				name: 'TSS',
				data: [
					<?php
						foreach ($item as $sensor) {
							$tss = round($sensor->tss,2);
							echo "$tss,";
						}
					?>
				]
			}],

			xaxis: {
				type: 'time',
				categories: [
					<?php
						foreach ($item as $sensor) {
							echo "'$sensor->jam',";
						}
					?>
				],
				axisBorder: {
					show: true,
					color: COLOR_SILVER_TRANSPARENT_5,
					height: 1,
					width: '100%',
					offsetX: 0,
					offsetY: -1
				},
				axisTicks: {
					show: true,
					borderType: 'solid',
					color: COLOR_SILVER,
					height: 6,
					offsetX: 0,
					offsetY: 0
				}             
			},
			tooltip: {
				x: {
					format: 'HH:mm'
				},
			}
		};
		
		var chart = new ApexCharts(
			document.querySelector('#apex-area-chart4'),
			options
		);

		chart.render();
	};

	var handleAreaChart5 = function() {
		"use strict";
		
		var options = {
			chart: {
				height: 350,
				type: 'line',
			},
			dataLabels: {
				enabled: true
			},
			stroke: {
				curve: 'smooth',
				width: 3
			},
			colors: [COLOR_PINK, COLOR_DARK],
			series: [{
				name: 'NH3-N',
				data: [
					<?php
						foreach ($item as $sensor) {
							$nh3n = round($sensor->nh3n,2);
							echo "$nh3n,";
						}
					?>
				]
			}],

			xaxis: {
				type: 'time',
				categories: [
					<?php
						foreach ($item as $sensor) {
							echo "'$sensor->jam',";
						}
					?>
				],
				axisBorder: {
					show: true,
					color: COLOR_SILVER_TRANSPARENT_5,
					height: 1,
					width: '100%',
					offsetX: 0,
					offsetY: -1
				},
				axisTicks: {
					show: true,
					borderType: 'solid',
					color: COLOR_SILVER,
					height: 6,
					offsetX: 0,
					offsetY: 0
				}             
			},
			tooltip: {
				x: {
					format: 'HH:mm'
				},
			}
		};
		
		var chart = new ApexCharts(
			document.querySelector('#apex-area-chart5'),
			options
		);

		chart.render();
	};

	var handleAreaChart6 = function() {
	"use strict";

	var options = {
	chart: {
		height: 350,
		type: 'line',
	},
	dataLabels: {
		enabled: true
	},
	stroke: {
		curve: 'smooth',
		width: 3
	},
	colors: [COLOR_PINK, COLOR_DARK],
	series: [{
		name: 'Temperature',
		data: [
            <?php
                foreach ($item as $sensor) {
					$suhu = round($sensor->suhu,2);
                    echo "$suhu,";
                }
            ?>
        ]
	}],

	xaxis: {
		type: 'time',
		categories: [
            <?php
                foreach ($item as $sensor) {
                    echo "'$sensor->jam',";
                }
            ?>
        ],
		axisBorder: {
			show: true,
			color: COLOR_SILVER_TRANSPARENT_5,
			height: 1,
			width: '100%',
			offsetX: 0,
			offsetY: -1
		},
		axisTicks: {
			show: true,
			borderType: 'solid',
			color: COLOR_SILVER,
			height: 6,
			offsetX: 0,
			offsetY: 0
		}             
	},
	tooltip: {
		x: {
			format: 'HH:mm'
		},
	}
	};

	var chart = new ApexCharts(
	document.querySelector('#apex-area-chart6'),
	options
	);

	chart.render();
	};

	var ChartApex = function () {
		"use strict";
		return {
			//main function
			init: function () {
				handleAreaChart();
				handleAreaChart2();
				handleAreaChart3();
				handleAreaChart4();
				handleAreaChart5();
				handleAreaChart6();
			}
		};
	}();

	$(document).ready(function() {
		ChartApex.init();
	});
</script>