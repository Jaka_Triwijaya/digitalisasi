@extends('source')
@section('begin')
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">SPARING KLHK DASHBOARD</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div id="flow" style="position: relative; width: 1250px;">
		
	</div>
	
		{{-- <div class="ml-5" style="position: relative; margin-top: -390px;">
			<div class="clock" style="position: relative; left: 920px;">
				<div class="hour hand" id="hour" style="margin-left: 115px;"></div>
				<div class="minute hand" id="minute" style="margin-left: 115px;"></div>
				<div class="seconds hand" id="seconds" style="margin-left: 118px;"></div>
		
				<img src="public/assets/img/clock template.svg" alt="clock" height="260px" width="260px" style="margin-left: -10px; margin-top: -10px">
			</div>
		</div>	 --}}
	
	<div id="chartt">
		
	</div>
	<div class="">
		
	</div>
	<!-- end row -->
	<div class="row" style="margin-top: 60px;">
		<div class="col-xl-6">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="chart-js-1">
				<div class="panel-heading">
					<h4 class="panel-title">Flow</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<p class="mb-0">
						{{-- With their mountain-like appearance, JavaScript Area Charts are used to represent quantitative variations. --}}
					</p>
				</div>
				<div class="panel-body p-0" >
					<div id="apex-area-chart"></div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<div class="col-xl-6">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="chart-js-2">
				<div class="panel-heading">
					<h4 class="panel-title">PH</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<p class="mb-0">
						{{-- With their mountain-like appearance, JavaScript Area Charts are used to represent quantitative variations. --}}
					</p>
				</div>
				<div class="panel-body p-0">
					<div id="apex-area-chart2"></div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<div class="col-xl-6">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="chart-js-3">
				<div class="panel-heading">
					<h4 class="panel-title">COD</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<p class="mb-0">
						{{-- With their mountain-like appearance, JavaScript Area Charts are used to represent quantitative variations. --}}
					</p>
				</div>
				<div class="panel-body p-0">
					<div id="apex-area-chart3"></div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<div class="col-xl-6">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="chart-js-4">
				<div class="panel-heading">
					<h4 class="panel-title">TSS</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<p class="mb-0">
						{{-- With their mountain-like appearance, JavaScript Area Charts are used to represent quantitative variations. --}}
					</p>
				</div>
				<div class="panel-body p-0">
					<div id="apex-area-chart4"></div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<div class="col-xl-6">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="chart-js-5">
				<div class="panel-heading">
					<h4 class="panel-title">NH3-N</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<p class="mb-0">
						{{-- With their mountain-like appearance, JavaScript Area Charts are used to represent quantitative variations. --}}
					</p>
				</div>
				<div class="panel-body p-0">
					<div id="apex-area-chart5"></div>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<div class="col-xl-6">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="chart-js-6">
				<div class="panel-heading">
					<h4 class="panel-title">Temperature</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<p class="mb-0">
						{{-- With their mountain-like appearance, JavaScript Area Charts are used to represent quantitative variations. --}}
					</p>
				</div>
				<div class="panel-body p-0">
					<div id="apex-area-chart6"></div>
				</div>
			</div>
			<!-- end panel -->
		</div>
	</div>

<!-- end #content -->
		
<!-- ================== BEGIN BASE JS ================== -->
<script src="public/assets/js/app.min.js"></script>
<script src="public/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>


<script>
	$(document).ready(function(){
		$('#flow').load("{{url('showpressure')}}");
		$('#chartt').load("{{url('chartrefresh')}}");
		setInterval(read,5000);
		setInterval(read_chart,60000);
		// 1800000

        function read() {
            $("#flow").load("{{url('showpressure')}}");
        }
		function read_chart() {
			$('#chartt').load("{{url('chartrefresh')}}");
		}
	});
	// $(document).ready(function(){
	// 	 setInterval(function() {
	// 		$('#flow').load("{{url('showpressure')}}");
	// 	 },5000);
	// });
</script>



@endsection
