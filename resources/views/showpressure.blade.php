<div class="row">
    <!-- begin col-3 -->
    <div class="col-xl-3 col-md-6">
        <div class="widget widget-stats bg-white text-inverse">
            <div class="stats-icon stats-icon-square bg-gradient text-white" style="background-color: #46244C"><i class="ion-ios-speedometer"></i></div>
            <div class="stats-content">
                <div class="stats-title text-inverse-lighter" style="font-size: 20px;">Flow</div>
                <div class="stats-number"><p>{{round($data->debit,2)}}</p><p style="font-size: 15px;">m3/menit</p></div>
                {{-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 70.1%;"></div>
                </div>
                <div class="stats-desc text-inverse-lighter">Better than last week (70.1%)</div> --}}
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-xl-3 col-md-6">
        <div class="widget widget-stats bg-white text-inverse">
            <div class="stats-icon stats-icon-square bg-gradient text-white" style="background-color: #FF6FB5"><i class="ion-ios-flask"></i></div>
            <div class="stats-content">
                <div class="stats-title text-inverse-lighter" style="font-size: 20px;">PH</div>
                <div class="stats-number"><p>{{round($data->ph,2)}}</p></div>
                <br><br>
                {{-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 70.1%;"></div>
                </div>
                <div class="stats-desc text-inverse-lighter">Better than last week (70.1%)</div> --}}
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-xl-3 col-md-6">
        <div class="widget widget-stats bg-white text-inverse">
            <div class="stats-icon stats-icon-square bg-gradient text-white" style="background-color: #5B7DB1"><i class="ion-ios-beaker"></i></div>
            <div class="stats-content">
                <div class="stats-title text-inverse-lighter" style="font-size: 20px;">COD</div>
                <div class="stats-number"><p>{{round($data->cod,2)}}</p><p style="font-size: 15px;">mg/L</p></div>
                {{-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 76.3%;"></div>
                </div>
                <div class="stats-desc text-inverse-lighter">Better than last week (76.3%)</div> --}}
            </div>
        </div>
    </div>
     <!-- begin col-3 -->
     <div class="col-xl-3 col-md-6">
        <div class="widget widget-stats bg-white text-inverse">
            <div class="stats-icon stats-icon-square bg-gradient text-white" style="background-color: #DEB6AB"><i class="ion-ios-chatboxes"></i></div>
            <div class="stats-content">
                <div class="stats-title text-inverse-lighter" style="font-size: 20px;">TSS</div>
                <div class="stats-number"><p>{{round($data->tss,2)}}</p><p style="font-size: 15px;">mg/L</p>	</div>
                {{-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 54.9%;"></div>
                </div>
                <div class="stats-desc text-inverse-lighter">Better than last week (54.9%)</div> --}}
            </div>
        </div>
    </div>
    <!-- end col-3 -->
</div>
<div class="row" style="margin-top: 20px;">
    <!-- begin col-3 -->
    <div class="col-xl-3 col-md-6">
        <div class="widget widget-stats bg-white text-inverse">
            <div class="stats-icon stats-icon-square bg-gradient text-white" style="background-color: #AB46D2"><i class="ion-ios-chatboxes"></i></div>
            <div class="stats-content">
                <div class="stats-title text-inverse-lighter" style="font-size: 20px;">NH3-N</div>
                <div class="stats-number"><p>{{round($data->nh3n,2)}}</p><p style="font-size: 15px;">mg/L</p>	</div>
                {{-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 54.9%;"></div>
                </div>
                <div class="stats-desc text-inverse-lighter">Better than last week (54.9%)</div> --}}
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-xl-3 col-md-6">
        <div class="widget widget-stats bg-white text-inverse">
            <div class="stats-icon stats-icon-square bg-gradient text-white" style="background-color: #9C0F48"><i class="ion-ios-thermometer"></i></div>
            <div class="stats-content">
                <div class="stats-title text-inverse-lighter" style="font-size: 20px;">Temperature</div>
                <div class="stats-number"><p>{{round($data->suhu,2)}}</p><p style="font-size: 15px;">Celcius</p></div>
                {{-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 54.9%;"></div>
                </div>
                <div class="stats-desc text-inverse-lighter">Better than last week (54.9%)</div> --}}
            </div>
        </div>
    </div>
    <!-- end col-3 -->
     <!-- begin col-3 -->
     <div class="col-xl-3 col-md-6">
        <div class="widget widget-stats bg-white text-inverse">
            <div class="stats-icon stats-icon-square bg-gradient text-white" style="background-color: #44d17c"><i class="ion-ios-analytics"></i></div>
            <div class="stats-content">
                <div class="stats-title text-inverse-lighter" style="font-size: 20px;">Totalizer</div>
                <div class="stats-number"><p>{{round($data->vol_limbah,2)}}</p><p style="font-size: 15px;">m3</p></div>
                {{-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 54.9%;"></div>
                </div>
                <div class="stats-desc text-inverse-lighter">Better than last week (54.9%)</div> --}}
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <div class="col-xl-3 col-md-6">
        <h3>Powered by :</h3> <br>
	    <img src="public/assets/img/logo/logo_kit.png" alt="" style="width: 300px;">
    </div>
</div>
<!-- end row -->