@extends('source')
@section('begin')
<!-- begin #content -->
<div id="content" class="content">
    <div id="js-map" class="map" style="margin-top: 8px;"></div>

	<div class="overlay-container">
		{{-- <span class="overlay-text" id="feature-name"></span> --}}
		<div class="overlay-text" id="feature-tenant"></div>
		<div class="overlay-text" id="feature-Debit"></div>
		<div class="overlay-text" id="feature-COD"></div>
	</div>
</div>
<!-- end #content -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="public/assets/OpenLayers/v6.14.1-dist/ol.js"></script>
<script src="public/assets/js/app.min.js"></script>
<script>
	// window.onload = init

	function init() {
		const map = new ol.Map({
			view: new ol.View({
				center: [-28272478.245948642, -668420.7377447269],
				zoom: 16,
				maxZoom: 19,
				minZoom: 8
				// rotation:
                //tes
			}),
			layers: [
				new ol.layer.Tile({
					source: new ol.source.OSM()
				})
			],
			target:"js-map"
		})
		// map.on('click', function(e){
		// 	console.log(e.coordinate);
		// })
		const openStreetMapStandard = new ol.layer.Tile({
			source: new ol.source.OSM(),
			visible: true,
			title: 'OSMStandard'
		})

		const openStreetMapHumanitarian = new ol.layer.Tile({
			source: new ol.source.OSM({
				url: 'https://a.tile.openstreetmap.fr/hot/${z}/${x}/${y}.png'
			}),
			visible: false,
			title: 'OSMHumanitarian'
		})

		const stamenTerrain = new ol.layer.Tile({
			source: new ol.source.XYZ({
				url: 'https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg',
				attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
			}),
			visible: false,
			title: 'StamenTerrain'
		})

		// Layer Group
		const baseLayerGroup = new ol.layer.Group({
  			layers: [
    			openStreetMapStandard, openStreetMapHumanitarian, stamenTerrain
			]
		})
		map.addLayer(baseLayerGroup);

		// Vector Layers
		const fillStyle = new ol.style.Fill({
			color: [84, 118, 255, 0.2]
		})

		const strokeStyle = new ol.style.Stroke({
			color: [4, 118, 255, 1],
			width: 1.2
		})

		const circleStyle = new ol.style.Circle({
			fill: new ol.style.Fill({
				color: [245, 49,5, 1]
			}),
			radius: 7,
			stroke: strokeStyle
		})

		const mapGeoJSON = new ol.layer.VectorImage({
			source: new ol.source.Vector({
				url: 'public/assets/location/poly.geojson',
				format: new ol.format.GeoJSON()
			}),
			visible: true,
			title: 'mapGeoJSON',
			style: new ol.style.Style({
				fill: fillStyle,
				stroke: strokeStyle,
			})
		})
		map.addLayer(mapGeoJSON);

		const markGeoJSON = new ol.layer.VectorImage({
			source: new ol.source.Vector({
				url: 'public/assets/location/map.geojson',
				format: new ol.format.GeoJSON()
			}),
			visible: true,
			title: 'markGeoJSON',
			style: new ol.style.Style({
				image: new ol.style.Icon({
					src: 'public/assets/img/logo/location.png'
				})
			})
		})
		map.addLayer( markGeoJSON);

		//Vector Feature Popup Logic
		const overlayContainerElement = document.querySelector('.overlay-container');
		const overlayLayer = new ol.Overlay({
			element: overlayContainerElement
		})
		map.addOverlay(overlayLayer);

		// const overlayFeaturName = document.getElementById('feature-name')
		const overlayFeaturTenant = document.getElementById('feature-tenant')
		const overlayFeaturDebit = document.getElementById('feature-Debit')
		const overlayFeaturCOD = document.getElementById('feature-COD')

		map.on('click', function(e){
			overlayLayer.setPosition(undefined);
			map.forEachFeatureAtPixel(e.pixel, function(feature, layer){
				let clickedCoordinate = e.coordinate;
				// let clickedFeaturName = "Location: " + feature.get('nama');
				let clickedFeaturTenant = "Tenant: " + feature.get('tenant');
				console.log(clickedFeaturTenant);
				overlayLayer.setPosition(clickedCoordinate)
				// overlayFeaturName.innerHTML = clickedFeaturName;
				overlayFeaturTenant.innerHTML = clickedFeaturTenant;
				overlayFeaturDebit.innerHTML = "Debit" + " = " + "234.57" + " m3/menit";
				overlayFeaturCOD.innerHTML = "COD" + " = " + "7.92" + " mg/L";
			},
			{
				layerFilter: function(layerCandidate){
					// return layerCandidate.get('title') === 'mapGeoJSON';
					return layerCandidate.get('title') === 'markGeoJSON';
				}
			})
		})
	}
	init();
</script>
@endsection
