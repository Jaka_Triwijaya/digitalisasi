<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Kawasan Industri Medan</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link rel="icon" href="public/assets/img/kim.jpg">
	<link href="public/assets/css/apple/app.min.css" rel="stylesheet" />
    <link href="public/assets/css/clock.css" rel="stylesheet" />
    <link href="public/assets/css/today.css" rel="stylesheet" />
	<link href="public/assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
	<link href="public/assets/plugins/jvectormap-next/jquery-jvectormap.css" rel="stylesheet" />
	<link href="public/assets/plugins/bootstrap-calendar/css/bootstrap_calendar.css" rel="stylesheet" />
	<link href="public/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
	<link href="public/assets/plugins/nvd3/build/nv.d3.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-autofill-bs4/css/autofill.bootstrap4.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-colreorder-bs4/css/colreorder.bootstrap4.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-keytable-bs4/css/keytable.bootstrap4.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-rowreorder-bs4/css/rowreorder.bootstrap4.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="public/assets/OpenLayers/v6.14.1-dist/ol.css">
	<link href="public/assets/css/default/invoice-print.min.css" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
	<link href="public/assets/plugins/@danielfarrell/bootstrap-combobox/css/bootstrap-combobox.css" rel="stylesheet" />
	<link href="public/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/tag-it/css/jquery.tagit.css" rel="stylesheet" />
	<link href="public/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
	<link href="public/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
	<link href="public/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css" rel="stylesheet" />
	<link href="public/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet" />
	<link href="public/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css" rel="stylesheet" />
	<link href="public/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css" rel="stylesheet" />
	<style>
		.datetime{
			
		}
		.date{
            font-size: 15px;
            font-weight: 1000;
        }

        .time{
            font-size: 30px; 
			font-weight: 600;
			text-align: center;
        }

		.overlay-container{
			background-color: #555;
			width: 150px;
			color: #fff;
			text-align: center;
			border-radius: 1px;
			padding: 2px 0;
			position: absolute;
			z-index: 1;
			bottom: 100%;
			left: 50%;
			margin-left: -80px;
		}
	</style>
</head>
<body  onload="initClock()" id="auto">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show">
		<span class="spinner"></span>
	</div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
                <img src="public/assets/img/kim.jpg" alt="" height="50px" width="100px" style="left: 5px;"/>
				<a href="home2" class="navbar-brand"><span class="navbar-logo"></span> <b class="mr-1">PT KAWASAN INDUSTRI MEDAN</b> </a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header --><!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<div style="margin-top: 0px; font-family: Helvetica">
					<div class="time">
						<span id="hour">00</span>:
						<span id="minutes">00</span>:
						<span id="seconds">00</span>
						<span id="period">AM</span>
					</div>
					<div class="date">
						<span id="dayname">Day</span>,
						<span id="month">Month</span>
						<span id="daynum">00</span>,
						<span id="year">Year</span>
					</div>
				</div>
			   <li class="dropdown navbar-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						{{-- <img src="" alt="" />  --}}
						{{-- <span class="d-none d-md-inline">Admin</span> 
						<b class="caret"></b> --}}
					</a>
					{{-- <div class="dropdown-menu dropdown-menu-right">
						<a href="javascript:;" class="dropdown-item">Edit Profile</a>
						<a href="javascript:;" class="dropdown-item"><span class="badge badge-danger pull-right">2</span> Inbox</a>
						<a href="javascript:;" class="dropdown-item">Calendar</a>
						<a href="javascript:;" class="dropdown-item">Setting</a>
						<div class="dropdown-divider"></div>
						<a href="javascript:;" class="dropdown-item">Log Out</a>
					</div> --}}
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="image">
								<img src="" alt="" />
							</div>
							<div class="info">
								<b class="caret pull-right"></b>Admin
								{{-- <small>Front end developer</small> --}}
							</div>
						</a>
					</li>
					<li>
						<ul class="nav nav-profile">
							<li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
							<li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
							<li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
						</ul>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav"><li class="nav-header">Navigation</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-water bg-red"></i>
							<span>Sparing</span>
						</a>
						<ul class="sub-menu">
							<li><a href="home_sparing">Dashboard</a></li>
							<li><a href="report_sparing">Report</a></li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-pulse bg-gradient-purple"></i>
							<span>Digitalisasi</span>
						</a>
						<ul class="sub-menu">
							<li><a href="home_digitalisasi">Dashboard</a></li>
							<li><a href="report_digitalisasi">Report</a></li>
							<li><a href="billing">Invoice</a></li>
						</ul>
					</li>

					<!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
					<!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
	    @yield('begin')
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-primary btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	{{-- <script src="assets/js/app.min.js"></script> --}}
	<script src="public/assets/js/theme/apple.min.js"></script>
	
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="public/assets/plugins/d3/d3.min.js"></script>
	<script src="public/assets/plugins/nvd3/build/nv.d3.min.js"></script>
	<script src="public/assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
	<script src="public/assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="public/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="public/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="public/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-autofill/js/dataTables.autofill.min.js"></script>
	<script src="public/assets/plugins/datatables.net-autofill-bs4/js/autofill.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-colreorder/js/dataTables.colreorder.min.js"></script>
	<script src="public/assets/plugins/datatables.net-colreorder-bs4/js/colreorder.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-keytable/js/dataTables.keytable.min.js"></script>
	<script src="public/assets/plugins/datatables.net-keytable-bs4/js/keytable.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-rowreorder/js/dataTables.rowreorder.min.js"></script>
	<script src="public/assets/plugins/datatables.net-rowreorder-bs4/js/rowreorder.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-select/js/dataTables.select.min.js"></script>
	<script src="public/assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script src="public/assets/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
	<script src="public/assets/plugins/datatables.net-buttons/js/buttons.colVis.min.js"></script>
	<script src="public/assets/plugins/datatables.net-buttons/js/buttons.flash.min.js"></script>
	<script src="public/assets/plugins/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script src="public/assets/plugins/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script src="public/assets/plugins/pdfmake/build/pdfmake.min.js"></script>
	<script src="public/assets/plugins/pdfmake/build/vfs_fonts.js"></script>
	<script src="public/assets/plugins/jszip/dist/jszip.min.js"></script>
	<script src="public/assets/js/demo/table-manage-combine.demo.js"></script>
{{-- 	
	select --}}
	<script src="public/assets/plugins/jquery-migrate/dist/jquery-migrate.min.js"></script>
	<script src="public/assets/plugins/moment/min/moment.min.js"></script>
	<script src="public/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
	<script src="public/assets/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
	<script src="public/assets/plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<script src="public/assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
	<script src="public/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="public/assets/plugins/pwstrength-bootstrap/dist/pwstrength-bootstrap.min.js"></script>
	<script src="public/assets/plugins/@danielfarrell/bootstrap-combobox/js/bootstrap-combobox.js"></script>
	<script src="public/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="public/assets/plugins/tag-it/js/tag-it.min.js"></script>
	<script src="public/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="public/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="public/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script src="public/assets/plugins/bootstrap-show-password/dist/bootstrap-show-password.js"></script>
	<script src="public/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
	<script src="public/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
	<script src="public/assets/plugins/clipboard/dist/clipboard.min.js"></script>
	<script src="public/assets/js/demo/form-plugins.demo.js"></script>
	
	{{-- <script src="{{url('assets/js/json.js')}}"></script> --}}
	<script type="text/javascript">
		function updateClock() {
			var now = new Date();
			var dname = now.getDay(),
				mo = now.getMonth(),
				dnum = now.getDate(),
				yr = now.getFullYear(),
				hou = now.getHours(),
				min = now.getMinutes(),
				sec = now.getSeconds(),
				pe = "AM";
	
				if(hou == 0) {
					hou = 12;
				}
				if(hou > 12) {
					hou = hou - 12;
					pe = "PM";
				}
	
				Number.prototype.pad = function(digits) {
					for(var n = this.toString(); n.length < digits; n = 0 + n);
					return n;
				}
	
				var months = ["January","February","March","April","May","June","July","August", "September","October","November","December"];
				var week = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
				var ids = ["dayname","month","daynum","year","hour","minutes","seconds","period"];
				var values = [week[dname],months[mo],dnum.pad(2),yr,hou.pad(2),min.pad(2),sec.pad(2),pe];
				for(var i = 0; i < ids.length; i++)
				document.getElementById(ids[i]).firstChild.nodeValue = values[i];
		}
	
		function initClock() {
			updateClock();
			window.setInterval("updateClock()", 1);
		}
	
	</script>
	<script>
        $(function(){
            $('a').each(function(){
                if ($(this).prop('href') == window.location.href) {
                    $(this).addClass('active'); $(this).parents('li').addClass('active');
                }
            });
        });
    </script>

	<!-- ================== END PAGE LEVEL JS ================== -->\
   
</body>
</html>