@extends('source')
@section('begin')
<!-- begin #content -->
<div id="content" class="content">
    <div id="js-map" class="map" style="margin-top: 8px;"></div>
   
	<div class="overlay-container">
		<span class="overlay-text" id="feature-name"></span>
	</div>
</div>
<!-- end #content -->
		
<!-- ================== BEGIN BASE JS ================== -->
<script src="public/assets/OpenLayers/v6.14.1-dist/ol.js"></script>
<script src="public/assets/js/app.min.js"></script>
<script>
	// window.onload = init
	
	function init() {
		const map = new ol.Map({
			view: new ol.View({
				center: [-28272478.245948642, -668420.7377447269],
				zoom: 16,
				maxZoom: 19,
				minZoom: 8,
				multiWorld: true
				// rotation:
			}),
			layers: [
				new ol.layer.Tile({
					source: new ol.source.OSM()
				})
			],
			target:"js-map"
		})
		// map.on('click', function(e){
		// 	console.log(e.coordinate);
		// })
		const openStreetMapStandard = new ol.layer.Tile({
			source: new ol.source.OSM(),
			visible: false,
			title: 'OSMStandard'
		})

		const openStreetMapHumanitarian = new ol.layer.Tile({
			source: new ol.source.OSM({
				url: 'https://a.tile.openstreetmap.fr/hot/${z}/${x}/${y}.png'
			}),
			visible: true,
			title: 'OSMHumanitarian'
		})

		const stamenTerrain = new ol.layer.Tile({
			source: new ol.source.XYZ({
				url: 'https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg',
				attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
			}),
			visible: false,
			title: 'StamenTerrain'
		})
		
		// Layer Group
		const baseLayerGroup = new ol.layer.Group({
  			layers: [
    			openStreetMapStandard, openStreetMapHumanitarian, stamenTerrain
			]
		})
		map.addLayer(baseLayerGroup);

		// Vector Layers
		const mapGeoJSON = new ol.layer.VectorImage({
			source: new ol.source.Vector({
				url: 'public/assets/location/map.geojson',
				format: new ol.format.GeoJSON()
			}),
			visible: true,
			title: 'mapGeoJSON'
			
		})
		map.addLayer(mapGeoJSON);
	}
	init();
</script>
@endsection
