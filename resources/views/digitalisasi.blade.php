@extends('source')
@section('begin')
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">DIGITALISASI KLHK DASHBOARD</h1>
	<!-- end page-header -->
	<!-- begin row -->
	<!-- begin col-10 -->
    <div class="col-xxl-10">
        <div class="panel panel-inverse">
            <!-- begin panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title">Data DIGITALISASI</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body" id="report">
                <table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle" style="width: 100%;">
                    <thead>
                        <tr>
                            {{-- <th width="1%">No</th> --}}
                            <th class="text-nowrap">Tanggal</th>
                            <th class="text-nowrap">Jam</th>
                            <th class="text-nowrap">Tenant</th>
                            <th class="text-nowrap">Flow</th>
                            <th class="text-nowrap">COD</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                        <tr class="odd gradeX">
                            {{-- <td width="1%" class="f-s-600 text-inverse">{{$no}}</td> --}}
                            <td>{{$dt->tanggal}}</td>
                            <td>{{$dt->jam}}</td>
                            <td>{{$dt->master_tenant->nama}}</td>
                            <td>{{$dt->debit}}</td>
                            <td>{{$dt->cod}}</td>
                        </tr>
                       
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end panel-body -->
        </div>
    </div>
    <!-- end col-10 -->
	<!-- end row -->
</div>
<!-- end #content -->
		
<!-- ================== BEGIN BASE JS ================== -->
<script>
	$(document).ready(function(){
		$('#report').load("{{url('table')}}");
		setInterval(read, 
            5000);

        function read() {
            $("#report").load("{{url('table')}}");
        }
	});
	// $(document).ready(function(){
	// 	 setInterval(function() {
	// 		$('#flow').load("{{url('showpressure')}}");
	// 	 },5000);
	// });
</script>
<script src="public/assets/js/app.min.js"></script>
@endsection
