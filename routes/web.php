<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Data;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/home2', [App\Http\Controllers\HomeController::class, 'index2'])->name('home2');
Route::get('home_sparing', [Data::class,'index2'])->name('home_sparing');
Route::get('report_sparing', [Data::class,'report_sparing'])->name('report_sparing');
Route::get('showpressure', [Data::class,'showpressure'])->name('showpressure');
Route::get('chartrefresh', [Data::class,'chartrefresh'])->name('chartrefresh');
Route::get('home_digitalisasi', [Data::class,'index_digitalisasi'])->name('home_digitalisasi');
Route::get('report_digitalisasi', [Data::class,'digitalisasi'])->name('digitalisasi');
Route::get('geo', [Data::class,'index_geo'])->name('geo');
Route::get('billing', [Data::class,'billing'])->name('billing');

