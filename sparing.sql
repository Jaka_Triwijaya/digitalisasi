/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : sparing

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 08/09/2022 14:18:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for digitalisasi
-- ----------------------------
DROP TABLE IF EXISTS `digitalisasi`;
CREATE TABLE `digitalisasi`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `tenant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cod` double NOT NULL,
  `debit` double NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `digitalisasi_tenant_foreign`(`tenant`) USING BTREE,
  CONSTRAINT `digitalisasi_tenant_foreign` FOREIGN KEY (`tenant`) REFERENCES `master_tenant` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of digitalisasi
-- ----------------------------
INSERT INTO `digitalisasi` VALUES (1, '2022-09-09', '14:12:26', 'K1-02', 7.2, 234.7, NULL, NULL);

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for master_tenant
-- ----------------------------
DROP TABLE IF EXISTS `master_tenant`;
CREATE TABLE `master_tenant`  (
  `kode` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`kode`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_tenant
-- ----------------------------
INSERT INTO `master_tenant` VALUES ('K1-01', 'JAYA NATALINDO, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-02', 'CHAROEN POKPHAND INDONESIA, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-03', 'GRAFIKA NUSANTARA, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-04', 'INDOGLOVE', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-05', 'INTAN HAVEA, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-06', 'IPI, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-07', 'MAJU SEJAHTERA LESTARI, CV', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-08', 'MEDAN CANNING, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-09', 'MUSIM MAS, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-10', 'RED RIBBON, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-11', 'RUDI TANDIAS/SAHABAT KITA, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-12', 'SHORBY, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-13', 'SOCI MAS, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-14', 'DAVID YANG/SUMBER PERKASA PLASTIK', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-15', 'TOBA SURIMI, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-16', 'UNION CONFECTIONRY, Ltd. PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K1-17', 'SO HUAN/ANUGERAH PRIMA INDONESIA, PT', 'KIM-I');
INSERT INTO `master_tenant` VALUES ('K2-01', 'BUMI MENARA INTERNUSA, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-02', 'CJ. FEED MEDAN, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-04', 'GLOBAL SEAFOOD INDUSTRY, PT/FRESHINDO SARI LAUTAN', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-05', 'INTRACO AGRO INDUSTRI', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-06', 'LEONG HUP JAYAINDO, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-07', 'LESTARI ALAM SEGAR, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-08', 'MEDAN BAJAINDO/MARTHIN THE', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-09', 'MEDAN SUGAR INDUSTRI, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-10', 'MUSIM MAS, PT (IPAL I)', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-11', 'MUSIM MAS, PT (IPAL II)', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-12', 'MUSIM MAS, PT (III), PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-13', 'OLEOCHEMICAL & SOAP INDUSTRY, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-14', 'PASIFIC MEDAN INDUSTRI, PT 1', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-15', 'PASIFIC MEDAN INDUSTRI, PT 2', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-16', 'PASIFIC PALMINDO, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-17', 'PHPO, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-18', 'MEDAN INDUSTRIAL PARK / ROYAL BREW HOUSE', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-19', 'SERBA GUNA, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-20', 'SUMATERA HAKARINDO, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-21', 'SURYA BUANA MANDIRI, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-22', 'SURYA MAS ABADI MAKMUR, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-23', 'TOBA SURIMI, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-24', 'MUTIARA LAUT ABADI', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-25', 'INTI SARI ATSIRI, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-26', 'UNIVERSAL INDOFOOD PRODUCT, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-27', 'WINSON PRIMA SEJAHTERA. PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K2-28', 'YAKITA MULIA, PT', 'KIM-II');
INSERT INTO `master_tenant` VALUES ('K3-02', 'GROWTH ASIA PLTU', 'KIM-III');
INSERT INTO `master_tenant` VALUES ('K3-03', 'GROWTH ASIA PLTU (Rubber Division)', 'KIM-III');
INSERT INTO `master_tenant` VALUES ('K3-04', 'INDOWANGI NUSAJAYA', 'KIM-III');
INSERT INTO `master_tenant` VALUES ('K3-05', 'JULIUS (KOMITAMA. PT)', 'KIM-III');
INSERT INTO `master_tenant` VALUES ('K3-06', 'VVF INDONESIA', 'KIM-III');
INSERT INTO `master_tenant` VALUES ('K3-07', 'MUSIM MAS, PT', 'KIM-III');
INSERT INTO `master_tenant` VALUES ('K4-02', 'INDO JAYA AGRINUSA', 'KIM-IV');
INSERT INTO `master_tenant` VALUES ('K4-03', 'WINSON PRIMA SEJAHTERA, PT', 'KIM-IV');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_06_20_171131_sensor', 1);
INSERT INTO `migrations` VALUES (6, '2022_06_21_062120_master_tenant', 1);
INSERT INTO `migrations` VALUES (7, '2022_06_21_113150_digitalisasi', 1);
INSERT INTO `migrations` VALUES (8, '2022_06_21_181624_jabatan', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for sensor
-- ----------------------------
DROP TABLE IF EXISTS `sensor`;
CREATE TABLE `sensor`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `ph` double NOT NULL,
  `nh3n` double NOT NULL,
  `cod` double NOT NULL,
  `tss` double NOT NULL,
  `debit` double NOT NULL,
  `suhu` double NOT NULL,
  `vol_limbah` double NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sensor
-- ----------------------------
INSERT INTO `sensor` VALUES (9, '2022-07-04', '18:15:00', 6.32, 7.19, 8.67, 159.12, 234.57, 25.67, 8937.3451, '2022-07-04 07:34:49', '2022-07-04 07:34:49');
INSERT INTO `sensor` VALUES (10, '2022-07-04', '18:16:00', 7.32, 8.19, 8.67, 139.12, 234.57, 25.67, 8937.3451, '2022-07-04 07:35:04', '2022-07-04 07:35:04');
INSERT INTO `sensor` VALUES (11, '2022-07-04', '19:15:00', 8.32, 7.19, 8.67, 189.12, 234.57, 25.67, 8937.3451, '2022-07-04 07:35:46', '2022-07-04 07:35:46');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
