<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\sensor;
use App\Models\digitalisasi;

class RestApi extends Controller
{
    //
    function post(Request $request) 
    {
        $sensor = new sensor;
        $sensor->tanggal = $request->tanggal;
        $sensor->jam = $request->jam;
        $sensor->ph = $request->ph;
        $sensor->nh3n = $request->nh3n;
        $sensor->cod = $request->cod;
        $sensor->tss = $request->tss;
        $sensor->debit = $request->debit;
        $sensor->suhu = $request->suhu;
        $sensor->vol_limbah = $request->vol_limbah;
        
        $sensor->save();
        
        return response()->json(
            [
                "message" => "Success",
                "data" => $sensor
            ]
        );
    }
    function post_digitalisasi(Request $request) 
    {
        $data = $request->input();
        foreach ($data['data'] as $key => $value) {
            $digitalisasi = new digitalisasi;
            $digitalisasi->tanggal = $value['tanggal'];
            $digitalisasi->jam = $value['jam'];
            $digitalisasi->tenant = $value['kode_tenant'];
            $digitalisasi->cod = $value['cod'];
            $digitalisasi->debit = $value['debit'];         
            $digitalisasi->save();
        }
        
        return response()->json(
            [
                "message" => "Success",
                "data" => $digitalisasi
            ]
        );
    }
}
