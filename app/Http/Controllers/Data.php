<?php

namespace App\Http\Controllers;

use App\Models\sensor;
use App\Models\Digitalisasi;
use App\Models\Master_tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Data extends Controller
{
    //
    public function index2() {
        $data = sensor::latest('id')->first();
        $now = date("Y-m-d");
        $all = sensor::orderBy("jam","asc")
        ->where('tanggal','$now')->get();
        return view('home2',compact('data','all'));
    }
    public function report_sparing()
    {
        $data = sensor::orderBy('tanggal','DESC')->get();
        return view('report_sparing',compact('data'));
    }
    public function showpressure()
    {
        $data = sensor::latest('id')->first();
        return view('showpressure',compact('data'));
    }
    public function chartrefresh()
    {   
        $now = date("Y-m-d");
        // $item = sensor::all()->distinct()->on($now,cast());
        // for postgre
        // $item = DB::select("SELECT * FROM (SELECT DISTINCT ON (tanggal,CAST(jam AS char(2))) id,tanggal,jam,debit,ph,cod,tss,nh3n,suhu FROM sensor) a WHERE a.tanggal = '$now'");
        // for mysql
        $item = DB::select("SELECT * FROM (SELECT tanggal,CAST(jam AS char(2)) id,jam,debit,ph,cod,tss,nh3n,suhu FROM sensor  WHERE  tanggal = '2022-07-04') a group by a.tanggal,a.id");

        // $item = sensor::orderBy("jam","asc")
        // ->where('tanggal','$now')->get();
        return view('chart',compact('item'));
    }
    public function digitalisasi()
    {
        $data = Digitalisasi::with('master_tenant')->orderBy('tanggal','DESC')->get();
        return view('digitalisasi',compact('data'));
    }
    public function index_digitalisasi()
    {
        return view('dashboard_digitalisasi');   
    }
    public function index_geo()
    {
        return view('geo');   
    }
    public function billing()
    {
        $data1 = Master_tenant::where('lokasi', 'KIM-I')->get();
        $data2 = Master_tenant::where('lokasi', 'KIM-II')->get();
        $data3 = Master_tenant::where('lokasi', 'KIM-III')->get();
        $data4 = Master_tenant::where('lokasi', 'KIM-IV')->get();
        return view('billing',compact('data1','data2','data3','data4'));
    }


}
