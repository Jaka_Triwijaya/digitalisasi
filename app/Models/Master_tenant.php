<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class master_tenant extends Model
{
    use HasFactory;
    protected $table = 'master_tenant';
    protected $primaryKey = "kode";
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'kode',
        'nama',
        'lokasi'
    ];
    public function digitalisasi() {
        return $this->hasMany(Digitalisasi::class);
    }
}
