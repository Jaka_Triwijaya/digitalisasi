<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sensor extends Model
{
    use HasFactory;
    protected $table = 'sensor';
    protected $primaryKey = "id";
    protected $fillable = [
        'tanggal',
        'jam',
        'ph',
        'nh3n',
        'cod',
        'tss',
        'debit',
        'suhu',
        'vol_limbah'
    ];
}
