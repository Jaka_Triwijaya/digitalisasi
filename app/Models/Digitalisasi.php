<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class digitalisasi extends Model
{
    use HasFactory;
    protected $table = 'digitalisasi';
    protected $primaryKey = "id";
    protected $fillable = [
        'tanggal',
        'jam',
        'tenant',
        'cod',
        'debit'
    ];
    public function master_tenant() {
        return $this->belongsTo(Master_tenant::class, 'tenant', 'kode');
    }
}
